```
env\Scripts\activate
cd object-detection-yolov5-fastapi/apis
pip install poetry
poetry shell
poetry install
pip install python-multipart
uvicorn app:app --reload
```

you can acces on http://127.0.0.1:8000/docs