import os
import logging
from io import BytesIO
import ssl
import torch
import uvicorn
from fastapi import FastAPI, Request, File, UploadFile, Query, HTTPException
from fastapi.responses import JSONResponse
from PIL import Image

from object_detection_yolov5.core.base_detector import ObjectDetector

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.StreamHandler()
file_handler = logging.FileHandler("api.log")
file_handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s : %(levelname)s : %(name)s : %(message)s")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

app: FastAPI = FastAPI()

model = torch.hub.load("ultralytics/yolov5", "yolov5s")

global_count_by_class = {"CROISSANT": 0, "GOLDENFIL": 0, "MILK": 0}

@app.post("/detect_and_count")
async def detect_and_count(request: Request, input_file: UploadFile = File(...), class_name: str = Query(..., title="Class Name", description="Name of the class to search for")):
    try:
        # Membaca gambar dari file yang diunggah
        image: Image = Image.open(BytesIO(await input_file.read()))

        # Membuat objek detector
        ob: ObjectDetector = ObjectDetector(image, model)

        # Melakukan deteksi objek
        json_results = ob.object_detect()

        # Cetak isi dari json_results
        print("json_results:", json_results)

        # Menghitung jumlah barang berdasarkan klasifikasi
        count_by_class = {"CROISSANT": 0, "GOLDENFIL": 0, "MILK": 0}  # Sesuaikan dengan nama kelas sesuai hasil cetakan json_results
        for detection_list in json_results['detections']:
            for result in detection_list:
                detected_class_name = result["class_name"]
                if detected_class_name in count_by_class:
                    count_by_class[detected_class_name] += 1

        # Simpan hasil deteksi ke variabel global
        global global_count_by_class
        global_count_by_class.update(count_by_class)

        # Menghitung total barang yang terdeteksi
        total_objects = sum(count_by_class.values())

        # Log informasi deteksi
        logger.info(["object detection results", json_results])

        # Validasi: Pastikan class_name berupa huruf kapital dan tidak kosong
        if not class_name.isalpha() or not class_name.isupper():
            raise HTTPException(
                status_code=400,
                detail="Invalid class name. Please provide a valid uppercase class name.",
            )

        # Dapatkan hasil deteksi dari variabel global
        count_by_class_global = global_count_by_class

        # Ambil jumlah barang terdeteksi berdasarkan class_name
        count_for_class = count_by_class_global.get(class_name, 0)

        # Log informasi deteksi
        logger.info([f"Product count for {class_name}", count_for_class])

        return JSONResponse(
            {
               #"data": json_results,
                #"count_by_class": count_by_class,
                #"total_objects": total_objects,
                "class_name": class_name,
                "count": count_for_class,
                "message": "object detected and product count retrieved successfully",
                "errors": None,
                "status": 200,
            },
            status_code=200,
        )
    except Exception as error:
        # Tangani kesalahan
        print(f"Error: {error}")
        exception_details = str(error)
        print(f"Exception details: {exception_details}")
        return JSONResponse(
            {
                "message": "object detection or product count failed",
                "errors": "error",
                "status": 400,
            },
            status_code=400,
        )

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
